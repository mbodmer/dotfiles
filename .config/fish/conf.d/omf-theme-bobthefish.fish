# https://github.com/oh-my-fish/theme-bobthefish

set -g theme_color_scheme dark
set -g theme_show_exit_status yes
set -g theme_display_user ssh
set -g fish_prompt_pwd_dir_length 0
set -g theme_project_dir_length 0
