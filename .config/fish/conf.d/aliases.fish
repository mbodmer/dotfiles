# ls
alias ls="exa"
alias ll="exa -l --git --time-style=iso --group-directories-first --classify"
alias la="exa -la --git --time-style=iso --group-directories-first --classify"
alias lg="exa -la --git --git-ignore --classify"
