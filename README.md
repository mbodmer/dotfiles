# Dotfiles

Personal dotfiles, managed using [Yet Another Dotfile Manager](https://yadm.io/)

## Setup

```
sudo apt install yadm
sudo apt install fish
sudo apt install fonts-powerline
sudo apt install exa
```

```
yadm clone https://codeberg.org/mbodmer/dotfiles.git
yadm bootstrap
```
